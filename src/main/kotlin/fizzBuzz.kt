package com.blancoja.fizzBuzzKata2

class fizzBuzz() {
    fun play(i: Int): String {

        return when (true) {
            i%15==0 -> "FIZZBUZZ"
            i%5==0 -> "BUZZ"
            i%3==0 -> "FIZZ"
            else -> i.toString()
        }
    }



}


fun main() {

}