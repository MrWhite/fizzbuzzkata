package com.blancoja.fizzBuzzKata2

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource


class unitTests {

    private val fizzBuzz = fizzBuzz()

    @Test
    fun shouldReturn1With1 (){

        assert(fizzBuzz.play(1).equals("1"))

    }

    @Test
    fun shouldReturn2With2 (){

        assert(fizzBuzz.play(2).equals("2"))
    }

    @Test
    fun shouldReturnFizzWith3 (){

        assert(fizzBuzz.play(3).equals("FIZZ"))
    }

    @Test
    fun shouldReturn4With4 (){

        assert(fizzBuzz.play(4).equals("4"))
    }

    @Test
    fun shouldReturnBuzzWith5 (){

        assert(fizzBuzz.play(5).equals("BUZZ"))
    }

    @Test
    fun shouldReturnFizzBuzzWith15 (){

        assert(fizzBuzz.play(15).equals("FIZZBUZZ"))
    }

    @Test
    fun shouldReturnFizzWith9 (){

        assert(fizzBuzz.play(9).equals("FIZZ"))
    }
    @Test
    fun shouldReturnBuzzWith25 (){

        assert(fizzBuzz.play(25).equals("BUZZ"))
    }

    @Test
    fun shouldReturnFizzBuzzWith60 (){

        assert(fizzBuzz.play(60).equals("FIZZBUZZ"))
    }

    @ParameterizedTest
    @ValueSource(ints = [3, 9])
    fun shouldReturnFizzWit(number: Int) {
        assert(fizzBuzz.play(number) == "FIZZ")
    }

    @ParameterizedTest
    @ValueSource(ints = [5, 25])
    fun shouldReturnBuzzWith25 (number: Int){
        assert(fizzBuzz.play(number) == "BUZZ")
    }

    @ParameterizedTest
    @ValueSource(ints = [15, 60])
    fun shouldReturnFizzBuzzWith (number: Int){
        assert(fizzBuzz.play(number) == "FIZZBUZZ")
    }
}
